package de.janisWorld;


import javax.sound.sampled.*;
import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.net.URL;

public class Main extends JFrame{

    public static Clip tusch, waitingGame, win, lose, entrence, waiting;

    public Main() {

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setVisible(true);
        setSize(500,500);
        setTitle("Soundboard");
        setLayout(new GridLayout(3,3));

        JButton btn1 = new JButton("Not Aus");
        btn1.setActionCommand("0000");
        btn1.addActionListener(new ButtonListener());
        btn1.setBackground(Color.RED);
        btn1.setForeground(Color.WHITE);
        JButton btn2 = new JButton("Entrance");
        btn2.setActionCommand("0001");
        btn2.addActionListener(new ButtonListener());
        JButton btn3 = new JButton("Tusch");
        btn3.setActionCommand("0010");
        btn3.addActionListener(new ButtonListener());
        JButton btn4 = new JButton("win");
        btn4.setActionCommand("0011");
        btn4.addActionListener(new ButtonListener());
        JButton btn5 = new JButton("lose");
        btn5.setActionCommand("0100");
        btn5.addActionListener(new ButtonListener());
        JButton btn6 = new JButton("waitingGame on");
        btn6.setActionCommand("0101");
        btn6.addActionListener(new ButtonListener());
        JButton btn7 = new JButton("waitingGame off");
        btn7.setActionCommand("0110");
        btn7.addActionListener(new ButtonListener());
        JButton btn8 = new JButton("waiting on");
        btn8.setActionCommand("0111");
        btn8.addActionListener(new ButtonListener());
        JButton btn9 = new JButton("waiting off");
        btn9.setActionCommand("1000");
        btn9.addActionListener(new ButtonListener());

        add(btn1);
        add(btn2);
        add(btn3);
        add(btn4);
        add(btn5);
        add(btn6);
        add(btn7);
        add(btn8);
        add(btn9);

        try {
            URL url = this.getClass().getClassLoader().getResource("tusch.wav");
            AudioInputStream audioIn = AudioSystem.getAudioInputStream(url);
            tusch = AudioSystem.getClip();
            tusch.open(audioIn);
        } catch (UnsupportedAudioFileException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (LineUnavailableException e) {
            e.printStackTrace();
        }

        try {
            URL url = this.getClass().getClassLoader().getResource("waitingGame.wav");
            AudioInputStream audioIn = AudioSystem.getAudioInputStream(url);
            waitingGame = AudioSystem.getClip();
            waitingGame.open(audioIn);
        } catch (UnsupportedAudioFileException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (LineUnavailableException e) {
            e.printStackTrace();
        }

        try {
            URL url = this.getClass().getClassLoader().getResource("win.wav");
            AudioInputStream audioIn = AudioSystem.getAudioInputStream(url);
            win = AudioSystem.getClip();
            win.open(audioIn);
        } catch (UnsupportedAudioFileException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (LineUnavailableException e) {
            e.printStackTrace();
        }

        try {
            URL url = this.getClass().getClassLoader().getResource("lose.wav");
            AudioInputStream audioIn = AudioSystem.getAudioInputStream(url);
            lose = AudioSystem.getClip();
            lose.open(audioIn);
        } catch (UnsupportedAudioFileException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (LineUnavailableException e) {
            e.printStackTrace();
        }

        try {
            URL url = this.getClass().getClassLoader().getResource("entrance.wav");
            AudioInputStream audioIn = AudioSystem.getAudioInputStream(url);
            entrence = AudioSystem.getClip();
            entrence.open(audioIn);
        } catch (UnsupportedAudioFileException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (LineUnavailableException e) {
            e.printStackTrace();
        }

        try {
            URL url = this.getClass().getClassLoader().getResource("waiting.wav");
            AudioInputStream audioIn = AudioSystem.getAudioInputStream(url);
            waiting = AudioSystem.getClip();
            waiting.open(audioIn);
        } catch (UnsupportedAudioFileException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (LineUnavailableException e) {
            e.printStackTrace();
        }


    }




    public static void main ( String[] args )
    {
        try
        {
            new Main();
        }
        catch ( Exception e )
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }


}
