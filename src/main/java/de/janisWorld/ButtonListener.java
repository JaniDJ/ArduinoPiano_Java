package de.janisWorld;

import javax.sound.sampled.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.URL;

public class ButtonListener implements ActionListener {
    public void actionPerformed(ActionEvent e1) {
        String ans = e1.getActionCommand();
        if(ans.contains("0000")){
            Main.win.stop();
            Main.entrence.stop();
            Main.lose.stop();
            Main.tusch.stop();
            Main.waitingGame.stop();
            Main.waiting.stop();
        }

        if (!(Main.win.isRunning() || Main.entrence.isRunning() || Main.lose.isRunning() || Main.tusch.isRunning() || Main.waitingGame.isRunning() || Main.waiting.isRunning())) {
            if(ans.contains("0001")){
                if (!Main.entrence.isRunning()) {
                    try {
                        URL url = this.getClass().getClassLoader().getResource("entrance.wav");
                        AudioInputStream audioIn = AudioSystem.getAudioInputStream(url);
                        Main.entrence = AudioSystem.getClip();
                        Main.entrence.open(audioIn);
                        Main.entrence.start();
                    } catch (UnsupportedAudioFileException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (LineUnavailableException e) {
                        e.printStackTrace();
                    }
                    System.out.println("Playing: entrance");
                }
            } else if(ans.contains("0010")){
                if (!Main.tusch.isRunning()) {
                    try {
                        URL url = this.getClass().getClassLoader().getResource("tusch.wav");
                        AudioInputStream audioIn = AudioSystem.getAudioInputStream(url);
                        Main.tusch = AudioSystem.getClip();
                        Main.tusch.open(audioIn);
                        Main.tusch.start();
                    } catch (UnsupportedAudioFileException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (LineUnavailableException e) {
                        e.printStackTrace();
                    }
                    System.out.println("Playing: tusch");
                }
            } else if(ans.contains("0011")){
                if (!Main.win.isRunning()) {
                    try {
                        URL url = this.getClass().getClassLoader().getResource("win.wav");
                        AudioInputStream audioIn = AudioSystem.getAudioInputStream(url);
                        Main.win = AudioSystem.getClip();
                        Main.win.open(audioIn);
                        Main.win.start();
                    } catch (UnsupportedAudioFileException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (LineUnavailableException e) {
                        e.printStackTrace();
                    }
                    System.out.println("Playing: win");
                }
            } else if(ans.contains("0100")){
                if (!Main.lose.isRunning()) {
                    try {
                        URL url = this.getClass().getClassLoader().getResource("lose.wav");
                        AudioInputStream audioIn = AudioSystem.getAudioInputStream(url);
                        Main.lose = AudioSystem.getClip();
                        Main.lose.open(audioIn);
                        Main.lose.start();
                    } catch (UnsupportedAudioFileException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (LineUnavailableException e) {
                        e.printStackTrace();
                    }
                    System.out.println("Playing: lose");
                }
            } else if(ans.contains("0101")){
                if (!Main.waitingGame.isRunning()) {
                    Main.waitingGame.loop(Clip.LOOP_CONTINUOUSLY);
                    Main.waitingGame.start();
                    System.out.println("Playing: waitingGame");
                }

            } else if (ans.contains("0111")) {
                if (!Main.waiting.isRunning()) {
                    Main.waiting.loop(Clip.LOOP_CONTINUOUSLY);
                    Main.waiting.start();
                    System.out.println("Playing: waiting");
                }

            }
        }

        if (ans.contains("0110")) {
            Main.waitingGame.stop();
            System.out.println("Paused: waitingGame");

        } else if (ans.contains("1000")) {
            Main.waiting.stop();
            System.out.println("Paused: waiting");

        }

    }
}
