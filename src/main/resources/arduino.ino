/*
  Simple example for receiving

  https://github.com/sui77/rc-switch/
*/

#include <RCSwitch.h>

RCSwitch mySwitch = RCSwitch();

void setup() {
  Serial.begin(9600);
  pinMode(2, OUTPUT);
  pinMode(5, OUTPUT);
  digitalWrite(2, HIGH);
  digitalWrite(5,LOW);
  mySwitch.enableReceive(1);  // Receiver on interrupt 0 => that is pin #2
}

void loop() {
  if (mySwitch.available()) {

    int value = mySwitch.getReceivedValue();

    if (value == 0) {
    } else {
      switch(mySwitch.getReceivedValue()){
        case 1115473:                           //A an
          Serial.print("0000\n");
          break;
        case 1115487:                           //A off
          Serial.print("0001\n");
          break;
        case 1118545:                           //B an
          Serial.print("0010\n");
          break;
        case 1118548:                           //B off
          Serial.print("0011\n");
          break;
        case 1119313:                           //C an
          Serial.print("0100\n");
          break;
        case 1119316:                           //C off
          Serial.print("0101\n");
          break;
        case 1119505:                           //D an
          Serial.print("0110\n");
          break;
        case 1119508:                           //D off
          Serial.print("0111\n");
          break;
      }
    }

    mySwitch.resetAvailable();
  }
}